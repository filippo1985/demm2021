import os

from flask import Flask
app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hello World$$!!'

@app.route('/esempio')
def esempio():
    return 'miii funziona!!'

if __name__=="__main__":
    port = int(os.getenv("WEBPORT",5000))
    host = os.getenv("WEBHOST",'0.0.0.0')

    app.run(host=host, port=port)